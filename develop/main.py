import os
import argparse
import io
import mimetypes

from yaml import parse
import PyPDF2
import pdf2image
import sheatless
import magic

from tests.sometest import sometest
from tests.contentbox import contentbox

os.umask(0) # Simplifies management stuff like deleting output files from the code editor on the host system.


def findFilePaths(base_directory, allowed_extensions, case_sensitive_extensions=False):
	"""
	Returns a list of paths to all files with an allowed extension in base_directory

	Example:
	-----------------------
	+-- input_pdfs
	|   +-- foo.pdf
	|   +-- bar.PDF
	|   +-- baz.png
	-----------------------
	findFilePaths("input_pdfs", [".pdf"])
	=> ["input_pdfs/foo.pdf", "input_pdfs/bar.PDF"]
	"""
	if not case_sensitive_extensions: allowed_extensions = [ext.lower() for ext in allowed_extensions]
	res = []
	for (dirpath, dirnames, filenames) in os.walk(base_directory):
		for filename in filenames:
			basename, extension = os.path.splitext(filename)
			if not case_sensitive_extensions: extension = extension.lower()
			if (extension in allowed_extensions):
				res.append(os.path.join(dirpath, filename))
	return res

def clear_dir(directory, recursive=True):
	for (dirpath, dirnames, filenames) in os.walk(directory):
		for filename in filenames:
			os.remove(os.path.join(directory, filename))
		if recursive:
			for dirname in dirnames:
				clear_dir(os.path.join(directory, dirname))
				os.rmdir(os.path.join(directory, dirname))

def tuplify_function_return(ret):
	if ret is None:
		return ()
	if type(ret) is not tuple:
		return (ret,)
	return ret


INPUT_PDF_DIR = "input_pdfs"
OUTPUT_PDF_DIR = "output_pdfs"
INPUT_IMG_DIR = "input_images"
OUTPUT_IMG_DIR = "output_images"
TMP_PATH = "tmp"

if not os.path.exists(INPUT_PDF_DIR):     os.mkdir(INPUT_PDF_DIR)
if not os.path.exists(OUTPUT_PDF_DIR):    os.mkdir(OUTPUT_PDF_DIR)
if not os.path.exists(INPUT_IMG_DIR):     os.mkdir(INPUT_IMG_DIR)
if not os.path.exists(OUTPUT_IMG_DIR):    os.mkdir(OUTPUT_IMG_DIR)
if not os.path.exists(TMP_PATH):          os.mkdir(TMP_PATH)

clear_dir(TMP_PATH)


class clear_output_action(argparse.Action):
	def __init__(self, option_strings, dest, **kwargs):
		return super().__init__(option_strings, dest, nargs=0, default=argparse.SUPPRESS, **kwargs)
	
	def __call__(self, parser, namespace, values, option_string, **kwargs):
		clear_dir(OUTPUT_IMG_DIR)
		clear_dir(OUTPUT_PDF_DIR)

formatter = lambda prog: argparse.ArgumentDefaultsHelpFormatter(prog, max_help_position=50)
parser = argparse.ArgumentParser(description="Develop and test sheatless", formatter_class=formatter)
parser.add_argument("--clear-output", action=clear_output_action, help="Clear output directories first")
parser.add_argument("operation", type=str, help="Name of python function to execute on each pdf page or image. The function recieves a bytes-like image and can return a list of tuples (descriptor, byte-like image) that should be outputted")
parser.add_argument("input_type", type=str, choices=["img", "pdf"], help="Select input type")
parser.add_argument("input", type=str, nargs="?", default="./", help="Path to input file or directory relative to input_images or input_pdfs.")
parser.add_argument("pages", type=str, nargs="*", default="all", help="Select which pages to analyze")
parser.add_argument("--engine", type=str, default="lstm", help="Options: lstm, legacy.")
parser.add_argument("--tessdata-dir", type=str, default="tessdata/tessdata_best-4.1.0/", help="Sets the TESSDATA directory for tesseract.")
args = parser.parse_args()

engine_kwargs = {
	"use_lstm": True if args.engine == "lstm" else False,
	"tessdata_dir": args.tessdata_dir,
}

if args.input_type == "img":
	if os.path.isfile(os.path.join(INPUT_IMG_DIR, args.input)):
		img_paths = [os.path.join(INPUT_IMG_DIR, args.input)]
	else:
		img_paths = findFilePaths(os.path.join(INPUT_IMG_DIR, args.input), [".png", ".jpg", ".jpeg", ".gif", ".bmp"])
	
	for img_path in img_paths:
		# Strip away INPUT_IMG_DIR and file extension from img_path
		output_path_small = os.path.join(*os.path.splitext(img_path)[0].split("/")[1:])
		# Join with OUTPUT_IMG_DIR to get path to output image directory for this image
		output_path = os.path.join(OUTPUT_IMG_DIR, output_path_small)
		if not os.path.exists(output_path): os.makedirs(output_path)

		# Here it is sometimes useful to clear the output path
		# clear_dir(output_path)

		with open(img_path, "rb") as file:
			output_imgs = tuplify_function_return(eval(f"{args.operation}(io.BytesIO(file.read()), engine_kwargs)"))
		
		for identifier, output_img in output_imgs:
			file_extension = mimetypes.guess_extension(magic.from_buffer(output_img.getvalue(), mime=True))
			with open(os.path.join(output_path, f"{identifier}{file_extension}"), "wb") as file:
				file.write(output_img.getbuffer())
		
elif args.input_type == "pdf":
	if os.path.isfile(os.path.join(INPUT_PDF_DIR, args.input)):
		pdf_paths = [os.path.join(INPUT_PDF_DIR, args.input)]
	else:
		pdf_paths = findFilePaths(os.path.join(INPUT_PDF_DIR, args.input), [".pdf"])

	for pdf_path in pdf_paths:
		# Strip away INPUT_PDF_DIR and file extension from pdf_path
		output_path_small = os.path.join(*os.path.splitext(pdf_path)[0].split("/")[1:])
		# Join with OUTPUT_IMG_DIR to get path to output image directory for this pdf
		output_path = os.path.join(OUTPUT_IMG_DIR, output_path_small)
		if not os.path.exists(output_path): os.makedirs(output_path)

		# Here it is sometimes useful to clear the output path
		# clear_dir(output_path)

		pdf = PyPDF2.PdfFileReader(pdf_path)
		page_nrs = range(pdf.getNumPages())
		if args.pages not in ["all", ["all"]]:
			page_nrs = [(int(page_nr) - 1) for page_nr in args.pages]
		for page_nr in page_nrs:
			pdf_writer = PyPDF2.PdfFileWriter()
			try:
				page = pdf.getPage(page_nr)
			except:
				print(f"WARNING: Page number {page_nr + 1} does not exist in {pdf_path}.")
				continue
			pdf_writer.addPage(page)
			page_bytes = io.BytesIO()
			pdf_writer.write(page_bytes)
			[page_img] = pdf2image.convert_from_bytes(page_bytes.getvalue())
			img_stream = io.BytesIO()
			page_img.save(img_stream, format="png")
			output_imgs = tuplify_function_return(eval(f"{args.operation}(img_stream, engine_kwargs)"))
			for identifier, output_img in output_imgs:
				file_extension = mimetypes.guess_extension(magic.from_buffer(output_img.getvalue(), mime=True))
				with open(os.path.join(output_path, f"page_{page_nr + 1}_{identifier}{file_extension}"), "wb") as file:
					file.write(output_img.getbuffer())
else:
	raise Exception(f"input_type {args.input_type} not recognized.")

