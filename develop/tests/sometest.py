import os
import io
import yaml
import sheatless
from PIL import Image

def sometest(img : io.BytesIO, engine_kwargs : dict):
    with open(os.path.join(os.path.dirname(__file__), "../sheatless/instruments.yaml")) as file:
        instruments = yaml.safe_load(file)
    data = sheatless.predict_parts_in_img(img, instruments, **engine_kwargs)
    print("data:", data)
    pil_img = Image.open(img)
    ret_stream = io.BytesIO()
    pil_img.save(ret_stream, format="jpeg")
    # return
    return ["bilde", ret_stream]
    return ["a", img], ["b", img]