# Todo

- Muligens splitte funnet tekst på /
- Detecte note-tittel

# Ferdig

- Sjekke matching av keywords med mellomrom
- Prøve å unngå at f eks baryton får treff på baritone saxophone
- Sjekke alle m-er som rn også (fiksa enkelt med flere entries i yaml-fila)
- Researche filopplasting
- Implementere filopplasting
- Definere interface mellom django og sheetmusicEngine
- Koble sammen django med sheetmusicEngine
